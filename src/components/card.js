import React, { Component } from "react";

class Card extends Component {

  handleKeyDown(e) {
    e.target.style.height = "inherit";
    e.target.style.height = `${e.target.scrollHeight}px`;
  }

  handleDragStart(e, cardDetails, columnName) {
    cardDetails.previousParent = columnName;
    e.dataTransfer.setData("text/plain", JSON.stringify(cardDetails));
  }

  render() {
    const { columnName, id, value, handleChangeCardValue } = this.props
    let cardDetails = { id: id, value: value };
    return (
      <div
        className="card"
        draggable="true"
        onDragStart={(e) => this.handleDragStart(e, cardDetails, columnName)}
      >
        <textarea
          draggable="false"
          className="cardField"
          placeholder={"Add card details"}
          defaultValue={value ? value : null}
          onKeyUp={this.handleKeyDown}
          onChange={(event) => {
            handleChangeCardValue(event, cardDetails, columnName);
          }}
        />
      </div>
    );
  }
}

export default Card;
