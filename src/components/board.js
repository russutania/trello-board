import React, { Component } from "react";
import { v4 as uuid } from 'uuid';
import { mapKeys } from 'lodash'
import Column from "./column.js";

class Board extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: {
        "to do": [
          {id: 1, value: "Call manager"},
          {id: 5, value: "Learn something new"},
        ],
        doing: [
          {id: 2, value: "Improve component"},
        ],
        done: [
          {id: 3, value: "Fix bug"},
        ],
      },
      order: ["to do", "doing", "done"],
      title: ''
    };
    this.handleChangeCardValue = this.handleChangeCardValue.bind(this);
  }

  componentWillMount() {
    this.localState = JSON.parse(localStorage.getItem("state"));
    this.setState({...this.localState });
  }

  componentDidUpdate() {
    let stringState = JSON.stringify(this.state);
    localStorage.setItem("state", stringState);
  }

  handleAddCard = (columnIndex) => {
    const {columns} = this.state
    let newCard = {};
    newCard.id = uuid();
    this.setState({
      columns: {
        ...columns,
        [columnIndex]: this.state.columns[columnIndex].concat(newCard)
      }
    })
  };

  handleChangeCardValue = (event, card, columnName) => {
    let idToReplace;
    const {columns} = this.state
    card.value = event.target.value;

    //check if the card is empty, if it is empty delete last item from columns array
    if (!card.value || card.value === "") {
      this.setState({
        columns: {
          ...columns,
          [columnName]: columns[columnName].pop()
        }
      });
    }

    let cloneState = Object.assign(columns[columnName]);
    columns[columnName].forEach((item, index) => {
      if (item.id === card.id) {
        idToReplace = index;
        cloneState[idToReplace] = card;
      }
    });

    this.setState({
      columns: {
        ...columns,
        [columnName]: cloneState
      }
    });
  };

  onHandleDrop = (e, columnName) => {
    e.preventDefault();
    e.stopPropagation();
    const {columns} = this.state
    let data = JSON.parse(e.dataTransfer.getData("text"));
    if (data.previousParent !== columnName) {
      //prevent the user to put drag again in the same column
      this.setState({
        columns: {
          ...columns,
          [data.previousParent]: columns[data.previousParent].filter((item) => item.id !== data.id),
          [columnName]: this.state.columns[columnName].concat(data)
        }
      });
    }
  };

  handleOnChange = (e) => {
    this.setState({title: e.target.value});
  }

  handleChangeTitle = (index, newTitle) => {
    const { order, columns } = this.state
    const newOrder = [...order]
    newOrder[index]= newTitle
    const newColumns = mapKeys(columns, function (value, key) {
      let newKey = key;
      if (key === order[index]) {
        newKey = newTitle
      }
      return newKey;
    })

    this.setState({
      order: newOrder,
      columns: newColumns
    })
  }

  handleAddColumn = () => {
    const {title, order, columns}= this.state
    const newOrder = [...order]
    newOrder.push(title)
    this.setState({
      columns: {
        ...columns,
        [title]: []
      },
      order: newOrder,
      title: ''
    })
  }

  render() {
    const { columns, title, order } = this.state

    return (
      <div className="board">
        {
          Object.entries(columns).map(([key, value], index) => {
            return <Column
              key={index}
              onHandleDrop={this.onHandleDrop}
              cards={value}
              handleAddCard={() => this.handleAddCard(order[index])}
              handleChangeCardValue={this.handleChangeCardValue}
              handleChangeTitle={this.handleChangeTitle}
              columnName={order[index]}
              columnIndex={index}
            />
          })
        }
        <div className="addNewColumn">
          <h3 className="addColumnTitle"> Add new column </h3>
          <input
            className="input"
            onChange={this.handleOnChange}
            value={title}
            placeholder={'Enter a title for this column'}
          />
          <button onClick={() => this.handleAddColumn()}>Add new column</button>
        </div>
      </div>
    );
  }
}

export default Board;
