import React, { Component } from "react";
import Card from "./card";

class Column extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isTitleEditable: false,
      newTitle: this.props.columnName ? this.props.columnName : ""
    }
  }

  handleDragOver = (e) => {
    e.preventDefault();
    e.stopPropagation();
  };

  onEditTitle = () => {
    this.setState({
      isTitleEditable: true
    })
  };

  onSaveTitle = () => {
    const {columnIndex, handleChangeTitle} = this.props;
    const {newTitle} = this.state;
    handleChangeTitle(columnIndex, newTitle);
    this.setState({
      isTitleEditable: false
    })
  };

  handleOnChange = (e) => {
    const { value } = e.target;
    this.setState({newTitle: value});
  };


  render() {
    const { onHandleDrop, columnName, cards, handleChangeCardValue, handleAddCard } = this.props
    const { isTitleEditable, newTitle } = this.state

    return (
      <div
        className="column"
        onDrop={(e) => onHandleDrop(e, columnName)}
        onDragOver={(e) => this.handleDragOver(e)}
      >
        <div className="titleWrapper">
          {
            isTitleEditable ?
              <input
                onChange={this.handleOnChange}
                className="inputTitle"
                value={newTitle}
                placeholder={'Enter a title of this column'}
              /> :
              <h4 className="title">
                {columnName}
              </h4>
          }
          <button className="editTitleButton" onClick={()=> isTitleEditable ? this.onSaveTitle() : this.onEditTitle()}> {isTitleEditable ? "Save" : "Edit"}</button>
        </div>
        {cards.map((card) => {
          return (
            <Card
              columnName={columnName}
              handleChangeCardValue={handleChangeCardValue}
              id={card.id}
              key={card.id}
              value={card.value}
            />
          );
        })}
        <button
          className="addCardButton"
          onClick={() => {
            handleAddCard();
          }}
        >
          Add a card
        </button>
      </div>
    );
  }
}

export default Column;
