
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### What I would like to add if I had spent more time with it.

-> functionality to attach files to the card,\
-> functionality to add comments for card

